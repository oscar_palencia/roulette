package RouletteJava;
import java.util.Random;
public class RouletteWheel {
    private Random rand;
    int number = 0;

    public RouletteWheel() {
        this.rand = new Random();
    }

    public void spin() {
        this.number = rand.nextInt(37);
    }

    public int getValue() {
        return this.number;
    }
}
