package RouletteJava;
import java.util.Scanner;

public class Roulette {
    public static void main(String[] args) {
        System.out.println("Welcome to Roulette!");

        RouletteWheel wheel = new RouletteWheel();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Would you like to bet?");
        String answer = scanner.next();

        if(answer.equals("no")) {
            System.out.println("Ok then. Have a good day!");
            System.exit(0);
        }

        System.out.println("How much would you like to bet? (How much money do you have)");
        int wallet = scanner.nextInt();

        System.out.println("What number would you like to bet on?");
        int numb = scanner.nextInt();

        System.out.println("How much would you like to bet this round?");
        int bet = scanner.nextInt();

        wheel.spin();
        

    }
}
